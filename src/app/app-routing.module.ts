import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { ChatComponent } from './component/chat/chat.component';
import { AuthLogoutGuard } from './common/guard/auth-logout.guard';
import { AuthLoginGuard } from './common/guard/auth-login.guard';
import { SignUpComponent } from './component/sign-up/sign-up.component';

const routes: Routes = [
  { path: 'login', canActivate: [AuthLoginGuard], component: LoginComponent },
  { path: 'signup', canActivate: [AuthLoginGuard], component: SignUpComponent },
  { path: 'chat', canActivate: [AuthLogoutGuard], component: ChatComponent },
  { path: '', redirectTo: '/login',  pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
