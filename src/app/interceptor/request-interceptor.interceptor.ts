import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { SessionService } from '../common/session/session.service';
import { AuthService } from '../common/auth/auth.service';
import { Router } from '@angular/router';
import { ToastService } from '../common/Toast/toast.service';

@Injectable()
export class RequestInterceptorInterceptor implements HttpInterceptor {

  constructor(
    private session: SessionService,
    private authService: AuthService,
    private router: Router,
    private toastService: ToastService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const token = this.session.User.Jwt;

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        this.authService._loader = false;

        switch (err.status) {
          case 401:
            this.authService._authErrorMsg = 'L\'adresse mail ou le mot de passe est incorrect.';
            this.router.navigate(['/login']);
            break;
          default:
            this.toastService.showErrorToast(err.error.Message);
            break;
        }
          return throwError(() => err);
        })
    );
  }
}
