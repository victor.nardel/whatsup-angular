import { Injectable } from '@angular/core';
import { User } from '../../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  public User = new User()
}
