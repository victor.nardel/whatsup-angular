import { Injectable } from '@angular/core';
import { Toast } from 'src/app/model/toast.model';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  public toasts: Toast[] = [];

  private addToast(toast: Toast): void {
    if (this.toasts.length > 4) {
      this.removeToast(this.toasts[0]);
    }

    this.toasts.push(toast);
    if (toast.closeAuto) {
      setTimeout(() => this.removeToast(toast), toast.timeout);
    }
  }

  removeToast(toast: Toast): void {
    var index: number = this.toasts.findIndex((i: Toast) => i.id == toast.id);
    if (index !== -1) {
      this.toasts.splice(index, 1);
    }
  }

  showSuccessToast(msg: string, timeout: number = 5000): void {
    this.addToast({
      id: this.generateId(),
      title: 'Succès !',
      faIcon: 'fa-circle-check',
      closeAuto: true,
      timeout: timeout,
      color: 'rgb(76, 173, 117)',
      innerContent: msg
    });
  }

  showErrorToast(msg: string): void {
    this.addToast({
      id: this.generateId(),
      title: 'Erreur !',
      faIcon: 'fa-circle-exclamation',
      closeAuto: false,
      timeout: 0,
      color: 'rgb(150, 47, 47)',
      innerContent: msg
    });
  }

  showAlertToast(msg: string, timeout: number = 10000): void {
    this.addToast({
      id: this.generateId(),
      title: 'Attention !',
      faIcon: 'fa-triangle-exclamation',
      closeAuto: false,
      timeout: timeout,
      color: 'rgb(205 116 28)',
      innerContent: msg
    });
  }

  showInfoToast(msg: string, timeout: number = 10000): void {
    this.addToast({
      id: this.generateId(),
      title: 'Notification !',
      faIcon: 'fa-circle-info',
      closeAuto: true,
      timeout: timeout,
      color: 'rgb(76, 102, 173)',
      innerContent: msg
    });
  }

  generateId(): string {
    return Math.random()
      .toString(36)
      .substr(2, 9);
  }
}
