import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SessionService } from '../session/session.service';
import { User } from 'src/app/model/user.model';
import { Observable, catchError, map, switchMap, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { Message } from 'src/app/model/message.model';
import { UrlConfig } from 'src/app/model/constant/url.constant';
import { ToastService } from '../Toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  _isLoggedIn: boolean = false;
  _redirectUrl: string | undefined;
  _loader: boolean = false;
  _socket!: WebSocket;
  _messagesList: Array<Message> = new Array<Message>;
  _connectedUser: Array<User> = new Array<User>;
  _recipient: User | undefined;
  _authErrorMsg: string | undefined;
  _isWriting: boolean = false;
  _isLoading = false;

  constructor(
    private http: HttpClient,
    private session: SessionService,
    private router: Router,
    private toastService: ToastService
  ) { 
    this._isLoggedIn = (localStorage.getItem('IsLoggedIn') === 'true');
    this.session.User.Jwt = localStorage.getItem('Jwt') || undefined;

    if (this._isLoggedIn && this.session.User.Jwt) {
      this.session.User.id = Number(localStorage.getItem('id'));
      this.session.User.Username = localStorage.getItem('Username');
      // this.session.User.UnreadMessages = Number(localStorage.getItem('UnreadMessages'));

      try {
        this.openSocket();
      } catch (err) {
        this.logout();
      }
    }
  }

  login(pEmail: string, pPassword: string): Observable<any> {

    var user = {
      Email: pEmail,
      Password: pPassword
    };

    return this.http.post<any>(`/api/user/login`, user).pipe(
      switchMap(res => {
        if (res && res.Jwt) {
          this._isLoggedIn = true;
          this.session.User.Jwt = res.Jwt;
          this.session.User.id = res.UserId;
          this.session.User.Username = res.Username;

          localStorage.setItem('IsLoggedIn', this._isLoggedIn.toString());
          localStorage.setItem('Jwt', this.session.User.Jwt ? this.session.User.Jwt : '');
          localStorage.setItem('id', this.session.User.id ? this.session.User.id.toString() : '');
          localStorage.setItem('Username', this.session.User.Username ? this.session.User.Username.toString() : '');
          // localStorage.setItem('UnreadMessages', this.session.User.UnreadMessages ? this.session.User.UnreadMessages.toString() : '');

          return this.http.post<any>(`/api/user/isconnected`, {Connected: true}).pipe(
            map(res => {
              this._authErrorMsg = '';
              if (res) {
                try {
                  this.openSocket();
                } catch (err) {
                  this.logout();
                }
                this._loader = false;
              }
            })
          );
        } else {
          this._loader = false;
          return throwError(() => "Invalid response from login API");
        }
      }),
      catchError(err => {
        this._loader = false;
        return throwError(() => err);
      })
    );
  }

  logout() {
    this._socket.close();

    localStorage.clear();
    this._isLoggedIn = false;
    this.router.navigate(['/login']);
  }

  openSocket(): void {
    this._socket = new WebSocket(`${UrlConfig.WSocketBaseUrl}/ws/open?userId=${this.session.User.id}`);
    this._socket.onopen = () => {
      const connected = {
        Type: 'Connected',
        id: this.session.User.id,
        Username: this.session.User.Username,
      }
      this._socket.send(JSON.stringify(connected));
    };

    this._socket.onmessage = async (e: MessageEvent) => {
      const text = await this.blobToString(e.data);
      const msg: any = JSON.parse(text);

      var userIndex;

      switch (msg.Type) {
        case "Private":
          if (msg.Id_Sender == this._recipient?.id)
            this._messagesList.push(msg);
          this._isWriting = msg.isWriting;
          if (msg.Id_Sender != this._recipient?.id) {
            const userIndex = this._connectedUser.findIndex(user => user.id === +msg.Id_Sender);
            if (userIndex !== -1) {
              this._connectedUser[userIndex].UnreadMessages++;
              this.toastService.showInfoToast(`Nouveau message de <strong class="notif ${msg.Id_Sender}">${msg.Username}</strong>`);
            }
          }
          break;
  
        case "Connected":
          userIndex = this._connectedUser.findIndex(user => user.id === +msg.id);
          if (userIndex !== -1)
            this._connectedUser.splice(userIndex, 1);

          this._connectedUser.push(msg);
          break;
  
        case "Disconnected":
          userIndex = this._connectedUser.findIndex(user => user.id === +msg.id);
          if (userIndex !== -1) this._connectedUser.splice(userIndex, 1);
          if (+msg.id === this._recipient?.id) this.emptyRecipient();
          break;

        case "IsWriting":
          if (msg.isWriting != undefined) this._isWriting = msg.isWriting;
          break;
      }

    };
  }

  changeRecipient(pUser: User | undefined): void {
    if (this._recipient == pUser) return;

    this._isLoading = true;
    this.emptyRecipient();
    if(pUser) this._recipient = pUser;

    if (this._recipient?.id) {
      var userBlock = document.getElementById(this._recipient.id.toString());
      if(userBlock) userBlock.style.backgroundColor = "#b8d4ca";
    }

    this._messagesList = new Array<Message>();
    this.http.get<any>(`/api/message/messages/${this.session.User.id}/${this._recipient?.id}`).subscribe(
      (result: any) => {
        this._isLoading = false;
        result.Messages.forEach((item: Message) => {
            this._messagesList.push(item);
        });

        // Enlever le compteur de messages non lu
        const userIndex = this._connectedUser.findIndex(user => user.id === pUser?.id);
        if (userIndex !== -1) 
          this._connectedUser[userIndex].UnreadMessages = 0;

      }
    );
  }

  emptyRecipient(): void {
    if (this._recipient?.id) {
      var userBlock = document.getElementById(this._recipient.id.toString());
      if(userBlock) userBlock.style.backgroundColor = "#eeeeee";
    }
    this._recipient = undefined;
  }

  blobToString(blob: Blob): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = (e: ProgressEvent<FileReader>) => {
        const text = e.target?.result as string;
        resolve(text);
      };

      reader.onerror = (e: ProgressEvent<FileReader>) => {
        reject(e.target?.error);
      };

      reader.readAsText(blob);
    });
  }
}
