import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
class PermissionsService {

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  canActivate(state: RouterStateSnapshot): boolean {
    if(this.authService._isLoggedIn) return true;

    this.authService._redirectUrl = state.url;
    this.router.navigate(['/login']);
    return false;
  }
}

export const AuthLogoutGuard: CanActivateFn = (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean => {
  return inject(PermissionsService).canActivate(state);
}
