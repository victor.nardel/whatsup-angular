import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './common/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { SessionService } from './common/session/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit   {

  constructor(
    private http: HttpClient,
    private authService: AuthService,
  ) { 
  }

  ngOnInit(): void {
    if (this.authService._isLoggedIn) {
      this.http.post<any>(`/api/user/isconnected`, {Connected: true}).subscribe(res => {});
    }
  }
}
