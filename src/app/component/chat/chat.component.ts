import { trigger, transition, style, animate } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/common/auth/auth.service';
import { SessionService } from 'src/app/common/session/session.service';
import { Message } from 'src/app/model/message.model';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0, height: '0', padding: '0' }),
        animate('50ms', style({ opacity: 1, height: '*', padding: '*' })),
      ]),
    ]),
  ]
})
export class ChatComponent implements OnInit, OnDestroy  {

  protected _content: string = '';
  protected _isOpenMenu: boolean = false;
  protected _writingTimeout: any;

  @ViewChild('textarea') textarea: ElementRef | undefined;
  @ViewChild('users') users: ElementRef | undefined;
  @ViewChild('toggleBtn') toggleBtn: ElementRef | undefined;

  constructor(
    protected authService: AuthService,
    protected session: SessionService,
    private http: HttpClient,
    private renderer: Renderer2,
  ) { }
  
  ngOnInit(): void {
    this.authService._recipient = undefined;

    this.authService._connectedUser = new Array<User>;
      this.http.get<any>('/api/user/users').subscribe(
        (result: any) => {
          result.result.forEach((item: any) => {
            if (item.id != this.session.User.id) {
              item.UnreadMessages = 0;
              this.authService._connectedUser.push(item);
            }
          });
        }
      );
  }

  ngOnDestroy(): void {
    if (this._writingTimeout) clearTimeout(this._writingTimeout);
  }

  @HostListener('window:click', ['$event'])
  @HostListener('document:keydown', ['$event']) // For fucking safari browser
  closeMenu(e: MouseEvent) {
    if (!this.users) return;
    if (!this.users.nativeElement.contains(e.target) && !this.toggleBtn?.nativeElement.contains(e.target))
      this.renderer.setStyle(this.users.nativeElement, 'right', '-230px');
  }

  openMenu(e: MouseEvent): void {
    if (!this.users) return;
    this.renderer.setStyle(this.users.nativeElement, 'right', '0');
  }

  adjustTextareaHeight(): void {
    if (!this.textarea) return;
    this.renderer.setStyle(this.textarea.nativeElement, 'height', 'auto');
    this.renderer.setStyle(
      this.textarea.nativeElement, 
      'height', 
      `calc(${this.textarea.nativeElement.scrollHeight}px - 20px)`
      );
  }

  sendMessage(): void {
    if (!this._content) return;

    var msg: Message = {
      Content: this._content,
      Id_Sender: this.session.User.id,
      Id_Recipient: this.authService._recipient?.id,
      Username: this.session.User.Username,
      createdAt: new Date(),
      Type: 'Private',
      isWriting: false
    };

    this.http.post<Message>('/api/message/send', msg).subscribe(res=> {});

    this.authService._messagesList.push(msg);

    this.authService._socket.send(JSON.stringify(msg));
    this._content = '';
    this.renderer.setStyle(this.textarea?.nativeElement, 'height', 'auto');
  }

  changeRecipient(pUser: User | undefined): void {
    this.authService.changeRecipient(pUser);
  }

  isWriting(): void {
    if(!this._content) return;

    if (this._writingTimeout) clearTimeout(this._writingTimeout);

    this._writingTimeout = setTimeout(() => {
      this.sendIsWriting(false);
    }, 5000);

    this.sendIsWriting(true);
  }

  sendIsWriting(pIsWriting: boolean): void {
    var jsonMsg: string = JSON.stringify({
      Id_Sender: this.session.User.id,
      Id_Recipient: this.authService._recipient?.id,
      Type: 'IsWriting',
      isWriting: pIsWriting
    });

    this.authService._socket.send(jsonMsg);
  }
}
