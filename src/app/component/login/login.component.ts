import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/common/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  protected _email: string = '';
  protected _password: string = '';

  constructor(
    protected authService: AuthService,
    private router: Router
  ){ }

  login(): void {
    this.authService._loader = true;
    this.authService.login(this._email, this._password).subscribe(
      (result: boolean) => {
        if (this.authService._isLoggedIn) {
          this.router.navigate(['/chat']);
        }
      }
    );
  }
}
