import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ToastService } from 'src/app/common/Toast/toast.service';
import { AuthService } from 'src/app/common/auth/auth.service';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent {

  protected _email: string = '';
  protected _username: string = '';
  protected _nom: string = '';
  protected _prenom: string = '';
  protected _password: string = '';
  protected _password_confirm: string = '';
  protected _ok: boolean = false;

  constructor(
    protected authService: AuthService,
    private toastService: ToastService,
    private http: HttpClient,
  ) {}

  signup(): void {

    if (this._email === '' || this._username === '') {
      this.toastService.showErrorToast('Tous les champs obligatoires doivent être remplis');
      return;
    }

    if (this._password_confirm !== this._password) {
      this._password = '';
      this._password_confirm = '';
      this.toastService.showErrorToast('Les deux mots de passe ne correspondent pas');
      return;
    }

    this.authService._loader = true;

    var newUser: any = {
      Username: this._username,
      FirstName: this._prenom,
      LastName: this._nom,
      Email: this._email,
      Password: this._password,
    }

    this.http.post<User>('/api/user/create', newUser)
    .subscribe(res => {
      if (res) {
        this._ok = true;
        this.authService._loader = false;
      }
    });
  }
}
