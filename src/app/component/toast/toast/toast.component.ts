import { animate, sequence, style, transition, trigger } from '@angular/animations';
import { Component, HostListener, ViewEncapsulation } from '@angular/core';
import { ToastService } from 'src/app/common/Toast/toast.service';
import { AuthService } from 'src/app/common/auth/auth.service';
import { AlertCode } from 'src/app/model/constant/enum.model';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css'],
  animations: [
    trigger('removeItem', [
      transition(':leave', [
        sequence([
          animate('150ms', style({ opacity: 0 })),
          animate('150ms', style({
            height: 0,
            paddingBottom: 0,
            paddingTop: 0
          }))
        ])
      ])
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ToastComponent {

  protected test: boolean = true;
  protected id: number = 0;

  constructor(
    protected toastService: ToastService,
    private authService: AuthService
  ){}

  @HostListener('click', ['$event.target'])
  onClick(target: any) {
    if (target instanceof HTMLElement && target.classList.length > 0) {
      if(target.classList[0] != 'notif') return;
      
      const senderId = target.classList[1];
      if (senderId) this.goToSelectedUser(+senderId);
    }
  }

  goToSelectedUser(pIdSender: number | undefined): void {
    this.authService.changeRecipient(
      this.authService._connectedUser.find(user => user.id = pIdSender)
    );
  }

  click(code: string) {
    switch (code) {
      case AlertCode.SUCCESS:
        this.toastService.showSuccessToast("Une erreur est survenu lors du chargement des données d'entrée ahaha");
        break;
      case AlertCode.ERROR:
        this.toastService.showErrorToast("Une erreur est survenu lors du chargement des données d'entrée ahaha");
        break;
      case AlertCode.ALERT:
        this.toastService.showAlertToast("Une erreur est survenu lors du chargement des données d'entrée ahaha");
        break;
      case AlertCode.INFO:
        this.toastService.showInfoToast("Une erreur est survenu lors du chargement des données d'entrée ahaha");
        break;
    }
  }
}
