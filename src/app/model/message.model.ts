import { WsResponse } from "./global.model";

export class Message extends WsResponse {
    public Id_Sender: number | undefined;
    public Id_Recipient: number | undefined;
    public Username: string | null | undefined;
    public Content: string | undefined;
    public createdAt: Date | undefined;
    public isWriting: boolean | undefined;
}