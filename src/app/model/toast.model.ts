export class Toast {
  id: string | undefined;
  title: string | undefined;
  faIcon: string | undefined;
  closeAuto: boolean | undefined;
  timeout: number | undefined;
  color: string | undefined;
  innerContent: string | undefined
}
