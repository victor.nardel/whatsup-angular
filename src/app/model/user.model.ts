export class User {
    public id: number | undefined;
    public Username: string | undefined | null;
    public FirstName: string | undefined | null;
    public LastName: string | undefined | null;
    public Email: string | undefined | null;
    public Password: string | undefined | null;
    public Jwt: string | undefined | null;
    public UnreadMessages: number = 0;
}